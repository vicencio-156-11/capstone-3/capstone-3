
import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext'; 
import { Navigate } from 'react-router-dom'; 

import Hero from './../components/Banner';
import {Form, Button, Container, Row, Col} from 'react-bootstrap';
import Swal from 'sweetalert2'; 



const data = {
  title: 'Welcome to SNA boutique! Please login.',
}

export default function Login() {

	const { user } = useContext(UserContext); 
   
    const [email, setEmail ] = useState('');	
    const [password, setPassword ] = useState(''); 

    let addressSign = email.search('@'); 
    let dns = email.search('.com');

    const [isActive, setIsActive] = useState(false);
    const [isValid, setIsValid] = useState(false);

   
    useEffect(() => {
		
    	if (dns !== -1 && addressSign !== -1 ) {
    		setIsValid(true);
    		if (password !== '') {
    			setIsActive(true);
    		} else {
    			setIsActive(false);
    		}
    	} else {
    		setIsValid(false); 
    		setIsActive(false); 
    	}
    },[email, password, addressSign, dns]) 

	
	const loginUser = async (event) => {
		event.preventDefault(); 
		
 
		fetch('https://fathomless-earth-11226.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => res.json())
		.then(dataNaJson => {
			
			let token = dataNaJson.accessToken; 
			

			
			if (typeof token !== 'undefined') {
				localStorage.setItem('accessToken',token);
				Swal.fire({
					icon: 'success',
					title: 'Login Successful',
					text: 'Welcome!'
				})	

				} else {
				Swal.fire({
					icon: 'error',
					title: 'Check your Credentials',
					text: 'Contact Admin if problem persist'
				})	
			}
		})

		
	};

	return(
	   user.id 
	   ?
	     <Navigate to="/products" replace={true}/>
	   :
	   <>
	   		<div id="loginBanner" >
					<Hero bannerData={data} />
				</div>
			<Container id="loginForm" >
				<Row>
						<Col xl={6}  xs={12} >
	    				<img id= "loginImg" src="/loginImg.png" alt=""/>
	    			</Col>

	    			<Col xl={6}  xs={12}>
							<h1 className="text-center pb-3">Login Form </h1>
							<Form onSubmit={e => loginUser(e)}>
							    {/*Email Address Field*/}
								<Form.Group className= "pb-3">
									<Form.Label>Email: </Form.Label>
									<Form.Control 
										type="email"
										placeholder="Enter Email Here"
										required
										value={email}
										onChange={event => {setEmail(event.target.value)} }
									/>
									{
										isValid ?
											<h6 className="text-success"> Email is Valid </h6>
										:
											<h6 className="text-mute"> Email is Invalid </h6>
									}
								</Form.Group>

								{/*Password Field*/}
								<Form.Group className= "pb-3">
									<Form.Label>Password: </Form.Label>
									<Form.Control 
										type="password"
										placeholder="Enter Password Here"
										required
										value={password}
										onChange={e => {setPassword(e.target.value)} }
									/>
								</Form.Group>

								{
									isActive ?
										<Button
										   
										  variant="outline-secondary"
										  type="submit"
										>
										 Login
										</Button>
									:
										<Button
										  
										  variant="outline-secondary"
										  disabled
										>
										 Login
										</Button>
								}
							</Form>
						</Col>
					</Row>

			</Container>

	   </>
	);
};
