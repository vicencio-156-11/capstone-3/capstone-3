// acquire all the component that will make up the home page (hero section, highlights)


// import Hero from './../components/Banner';
import {Container,Row, Col} from 'react-bootstrap';



// const data = {
// 	title: 'Welcome to ARMV University',
// 	content: 'Knowledge within our grasp'
// }




export default function Home() {
	return(
		<div>
			<div className="landing container-fluid d-sm-flex justify-content-between">
	    		<img id= "banner2" src="/bannerlogo.png"  alt=""/>
			</div>	
			<br/>
		    <Container className="row justify-content-left ml-5 pb-3" >
		    	<Row>
		    		<Col>
				      <a href="https://web.facebook.com/snaboutiqueonline?_rdc=1&_rdr">
				      	<img class= "social" src="/facebook.png"  alt=""/>
				      </a>
				    </Col>
				    <Col>
				      <a href="#https://www.instagram.com/snaboutiqueonline/">
				      	<img class= "social" src="/insta.png"  alt=""/>
				      </a>
				     </Col>
				     <Col>
				      <a href="#https://www.tiktok.com/@snaboutiqueonline"> 
				      <	img class= "social" src="/tiktok.png"  alt=""/>
				      </a>
				     </Col>
			     </Row>
		   	</Container>
		   	<div className="row justify-content-left ml-5 pb-3">
		   		© 2022, SNA boutique
		   	</div>
		</div>

		);
};


