
import {useState, useEffect} from 'react';
import ProductCard from './../components/ProductCard';
import {Container,Row, Col} from 'react-bootstrap';
import Hero from './../components/Banner';


const bannerDetails = {
	title: 'NEW ARRIVALS',
	content: 'Carry your own glimmer everywhere you go.'
}

export default function Products () {
	
	const [productsCollection, setProductCollection] = useState([]);
	useEffect(() => {
		
		fetch('https://fathomless-earth-11226.herokuapp.com/products/')
		.then(res => res.json())
		.then(convertedData => {
			setProductCollection(convertedData.map(product => {
				return(
					<ProductCard key={product._id} productProp={product}/>	
				) 
			}))
		});
	},[]);

	return(
	<>
		
		<div id="shopBanner">
		<Hero bannerData={bannerDetails}/>
		</div>
		<Container>
			<Row>
				<Col>
					{productsCollection}
				</Col>
			</Row>
		</Container>		
	</>
	);
};