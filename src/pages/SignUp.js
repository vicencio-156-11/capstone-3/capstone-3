
// identify the components needed to create the register page.

import {useState, useEffect} from 'react'
import Hero from '../components/Banner';
import {Container, Form, Button, Row, Col} from 'react-bootstrap';
import Swal from 'sweetalert2';



const data = {
	title: 'Create your SNA account',
	content: 'Access your favorite jewelry products with just one login.'
}


export default function SignUp() {

	
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo]= useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState(''); //password confirmation

	
	
	const [isActive, setIsActive] = useState(false);
	
	
	
	const [isMatched, setIsMatched] = useState(false);
	const [isMobileValid, setIsMobileValid] = useState(false);
	
	const [isAllowed, setIsAllowed] = useState(false);

	
	useEffect(() => {
		if (
			mobileNo.length === 11
		)
			{	
				setIsMobileValid(true)
				setIsActive(true);
				if (
					password1 === password2 && 
					password1 !== '' && password2 !== ''
				) {
					setIsMatched(true);
					if (firstName !== '' && lastName !== '' && email !== ''){
					setIsAllowed(true);
					setIsActive(true);
					} else {
					setIsAllowed(false);
					setIsActive(false);
					}
				} else {
					setIsMatched(false);
					setIsAllowed(false);
					setIsActive(false);
				}
			}
		else if(password1 !=='' && password1 === password2){
			setIsMatched (true);
		}
		else {
			setIsActive(false);
			setIsMatched(false);
			setIsMobileValid(false);
			setIsAllowed(false);
		};
	},[firstName,lastName, email, mobileNo, password1, password2]);

	const registerUser = async (eventSubmit) => {
		eventSubmit.preventDefault()
		setFirstName('');
		setLastName('')	;	
		setEmail('');
		setMobileNo('');
		setPassword1('');
		setPassword2('');

			await Swal.fire({
				icon:'success',
				title: 'Registration Successful',
				text: 'Thank you for creating an account.'
			})

			window.location.href = "/login";
	};


	return(
		<div>
			<div id="registerBanner2">
				<Hero bannerData={data}/>
			</div>
			
			
			<Container id="registerForm">
	    		<Row>
	    			<Col xl={6}  xs={12} className="pb-5 ">
	    				<img id= "registerImg" src="/signUp.png" alt=""/>
	    			</Col>
	    		
	    			<Col xl={6} xs={8}  id="signupForm">
						{/*	Form Heading*/}
						{
							isAllowed ?
								<h1 className="text-center">You May Now Sign Up!</h1>
							:
								<h1 className="text-center">Sign Up</h1>
						}
						<h6 className="text-center  mt-3 ">Fill Up the Form Below</h6>

						{/*Form*/}
							<Form  id="form" lg={6} onSubmit={e => registerUser(e)}>
								<Form.Group>
									<Form.Label>First Name:</Form.Label>
									<Form.Control type="text" placeholder="Enter your First Name" required value={firstName} onChange={event => setFirstName(event.target.value)} />
								</Form.Group>
									<Form.Label>Last Name:</Form.Label>
									<Form.Control type="text" placeholder="Enter your Last Name" required value={lastName} onChange={e => setLastName(e.target.value)}/>
			                    <Form.Group>
			                    	<Form.Label> Email:</Form.Label>
			                    	<Form.Control type="email" placeholder="Insert your EmailAddress" required value={email} onChange={e => setEmail (e.target.value)} />
			                    </Form.Group>
			                    {/*Customize this component to get the correct format for the mobile number*/}
			                    <Form.Group>
			                    	<Form.Label>Mobile Number:</Form.Label>
			                    	<Form.Control type="number" placeholder="Insert your mobile No." required value={mobileNo} onChange={e => setMobileNo (e.target.value)}/>
			                    		{
			                    			isMobileValid ?
			                    				<span className="text-success">Mobile Number is Valid!</span>
			                    			:
			                    				<span className="text-muted">Mobile Number Should be 11-digits</span>
			                    		}
			                    </Form.Group>
			                    <Form.Group>
			                    	<Form.Label>Password:</Form.Label>
			                    	<Form.Control type="password" placeholder="Enter your password" required value={password1} onChange={e => setPassword1 (e.target.value)}/>
			                    </Form.Group>

			                	{/*Confirm Password*/}
			                    <Form.Group>
			                    	<Form.Label>Confirm Password:</Form.Label>
			                    	<Form.Control type="password" placeholder="Confirm your password" required value={password2} onChange={e => setPassword2 (e.target.value)}/>
			                    		{
			                    			isMatched ?
			                    				<span className="text-success">Password Matched!</span>
			                    			:
			                    				<span className="text-danger">Passwords Should Match!</span>
			                    		}
			                    </Form.Group>

		                    {/*Register Button*/}
		                    {
		                    	isActive ? 
				                    <Button variant="outline-secondary"  type="submit" onClick> Sign Up </Button>
				                    	:
				                    <Button variant="outline-secondary"  disabled> Sign Up </Button>
		                    		
		                    }
							</Form>
						</Col>	
					</Row>
			</Container>
		</div>
	);
};


