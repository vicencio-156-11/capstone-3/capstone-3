
import { useState, useEffect} from 'react';
// import Hero from './../components/Banner';

import {Row, Col, Card, Button,Container} from 'react-bootstrap';

import Swal from 'sweetalert2';

import {Link, useParams} from 'react-router-dom';

// const data = {
// 	title: 'Knowledge within our grasp.',
// 	content: 'Start your journey to succes with ARMV University.'
// }

export default function ProductView(){

	
	const [productInfo, setProductInfo] = useState({
		name: null ,
		description: null ,
		price: null
	});

	
	console.log(useParams());
	const {id} = useParams()
	// console.log(id);

	
	useEffect(() => {
		fetch(`https://fathomless-earth-11226.herokuapp.com/products/${id}`).then(res => res.json()).then(convertedData => {
			console.log(convertedData);
			setProductInfo({
				name:  convertedData.name,
				description:  convertedData.description,
				price: convertedData.price
			})
		});
	},[id])
	

	const shop = () => {
		return(
			Swal.fire({
				icon: "success",
				title: 'This item has been added to your cart.',
				text: 'Thank you.'
			})
		);
	};

	const photos = require.context('./../../public/earrings', true);
	let productImg = photos(`./${id}.jpg`);


	return(
		<Container id="productView">
			{/*<Hero bannerData={data} />*/}
			<Row >
				<Col >
					{/*<Container id="singleProduct">*/}
						<Card className="text-center">
							<Card.Body id="singletext">
								<img className="earrings" src={productImg} alt="" />
							</Card.Body>
						</Card>
				</Col>
				<Col className="cardDesc">
						<Card className=" text-center">
							<Card.Body id="singletext">
							<Card.Title>
								<h2> {productInfo.name}</h2>
							</Card.Title>
							{/*<Card.Subtitle>
								<h6 className="my-3">Description:</h6>
							</Card.Subtitle>*/}
							<Card.Text>
								{productInfo.description}
							</Card.Text>
							{/*<Card.Subtitle>
									<h6 className="my-3">Price:</h6>
							</Card.Subtitle>*/}
								<Card.Text>
									PHP: {productInfo.price}
								</Card.Text>
							</Card.Body>
							
						</Card>
							<Button  style={{width:170, marginLeft:200}} variant="outline-secondary" id="single1" onClick={shop}>
								Add to Cart
							</Button>
							<br/>
							<Button style={{width:170, marginLeft:200}} variant="outline-secondary" id="single2" >
								<Link id="single2" to="/login">
									Login to Shop
								</Link>
							</Button>
					{/*</Container>*/}
				</Col>
			</Row>
		</Container>

	);
};