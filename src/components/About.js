// [ACTIVITY]


// [TASK]
// Create Responsive  component that will serve as an about page for the poject author


// Component: Bootsrtrap Grid System
// Title: About Me ()
// Full Name: Author
// Job Description: Full Stack Web Developer
// Description: 'About Yourself'
// Contacts: (Email, Mobile No, Address)

import {Row, Col} from 'react-bootstrap';

// we will use default bootstrap utility classes to fromat the component

// create a function that will describe the structure of the hero section

// 'class' -> reserved keyword (HTML)
// React/JSX elements -> 'className'
export default function About() {
	return(
		
		<Row className= "p-5 aboutMe">
			<Col xs={12} md={6}>
				<h2 className= "my-4">About Me</h2>
				<h3>Aubrey Rose M. Vicencio</h3>
				<h5>Full Stack Web Developer</h5>
				<p className= "my-3">I'm a Full Stack Web Developer. In charge of developing front end website architecture, designing user interactions on web pages, developing back-end website applications and creating servers and databases for functionality.</p>
				<h3>Contact</h3>
					<ul>
						<li>Email: vicencioaubrey@gmail.com</li>
						<li>Mobile No.: +639 1235 67876</li>
						<li>Address: Valenzuela City, Philippines</li>
					</ul>
			</Col>
		</Row>
	);
};