// identify needed components
import {Card} from 'react-bootstrap'

import {Link} from 'react-router-dom';


export default function ProductCard({productProp}){
	const photos = require.context('./../../public/earrings', true);
	let productImg = photos(`./${productProp._id}.jpg`);
	return(
			<Card className= 'productCard m-4 d-md-inline-flex d-sm-inline-flex'>
				<img id="earringsCard" src={productImg} alt="" />
				<Card.Body className= 'itemCard' >
					<Card.Title>
						{productProp.name}
					</Card.Title>
				
				
					<Card.Text>
						{productProp.description}
					</Card.Text>
				
				
					<Card.Text>
						Price: {productProp.price}
					</Card.Text>
				</Card.Body>
					<Link to={`view/${productProp._id}`} id="viewProduct" className = " btn pd-3" >
						View Product
					</Link>
			</Card>
	);
};