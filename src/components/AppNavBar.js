import { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap'


// implement Links in the navbar
import {Link} from 'react-router-dom';

// Consumer
import UserContext from '../UserContext';


function AppNavbar() {

	const {user} = useContext(UserContext);
	
	return(
		<>
			<Navbar id="navbar" expand="lg" fixed="top" > 
				<Container id="colortext">
					<Navbar.Brand>  </Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav"/>
					<Navbar.Collapse className="row justify-content-start">
						<Nav >
							<Link to="/" className="test">HOME</Link>
							{
								user.id !== null?
									<Link to="/logout" className="test">
									LOGOUT
									</Link>
								:
								<>
									<Link to="/signup" className="test">
									SIGN UP
									</Link>
								
									<Link to="/login" className="test">
										<img id= "loginClipart" src="/login.png"  alt=""/>
									</Link>
								</>
							}
							
							<Link to="/products"  className="test ">SHOP</Link>
						</Nav>
					</Navbar.Collapse>

					<Navbar.Collapse className="row justify-content-center"> 
							<img id= "logo" src="/logo.png"  alt=""/>	
					</Navbar.Collapse>

					<Navbar.Collapse className="row justify-content-end">
						<Nav >
							<Link to="/create" className="test ">ADD PRODUCT</Link>
							<Link to="/update" className="test ">UPDATE PRODUCT</Link>
						</Nav>
					</Navbar.Collapse>
				</Container>		
			</Navbar>

	{/*<br />
		  <Navbar bg="light" variant="light" fixed="bottom">
		    <Container>
		    
		    <Nav className="me-auto">
		      <Nav.Link href="https://web.facebook.com/snaboutiqueonline?_rdc=1&_rdr">
		      	Facebook
		      </Nav.Link>
		      <Nav.Link href="https://www.instagram.com/snaboutiqueonline/">
		      Instagram
		      </Nav.Link>
		      <Nav.Link href="https://www.tiktok.com/@snaboutiqueonline">
Tiktok
		      </Nav.Link>
		    </Nav>
		    </Container>
		  </Navbar>*/}
	</>

	);
};

export default AppNavbar; 