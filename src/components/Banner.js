// This component will be used as a hero section of our page
// Responsive -> grid system
import {Row, Col} from 'react-bootstrap';

// we will use default bootstrap utility classes to fromat the component

// create a function that will describe the structure of the hero section

// 'class' -> reserved keyword (HTML)
// React/JSX elements -> 'className'
export default function Banner({bannerData}) {
	return(
		<Row className= "p-5">
			<Col className= "banner">
				<h1> {bannerData.title} </h1>
				<p className="my-3">{bannerData.content}</p>
				{/*<a id="button1" className="btn" href="/">INSERT ACTION HERE</a>*/}
			</Col>
		</Row>
	);
}

 //expose the component