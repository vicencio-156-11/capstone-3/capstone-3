import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar';

// acquire the pages that will make up the app
import Home from './pages/Home';
import SignUp from './pages/SignUp'
import LoginPage from './pages/Login';
import Products from './pages/Products';
import ErrorPage from './pages/Error';
import ProductView from './pages/ProductView';
import LogoutPage from './pages/Logout';
import Create from './pages/Create';
import UpdateProduct from './pages/UpdateProduct';



import {UserProvider} from './UserContext';


import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

import './App.css';

function App() {
  const [user, setUser] =  useState({
    id: null,
    isAdmin: null
  }) 
  const unsetUser = () => {
  
  localStorage.clear();
  setUser({
    id: null,
    isAdmin: null
  });
}

useEffect(() => {
  
  let token = localStorage.getItem('accessToken')
  fetch('https://fathomless-earth-11226.herokuapp.com/users/details', {
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
  .then(res => res.json())
  .then(convertedData => {
   
    if(typeof convertedData._id !== "undefined") {
      setUser({
        id: convertedData._id,
        isAdmin: convertedData.isAdmin
      });
    } else {
        setUser({
          id: null,
          isAdmin: null
        });
      }
  });
},[user]);
  return (
   <UserProvider value= {{user,setUser, unsetUser}}>
      <Router>
        <AppNavBar/>
        <Routes>
          <Route path='/' element={<Home/>} />
          <Route path='/signup' element={<SignUp/>} />
          <Route path='/login' element={<LoginPage/>} />
          <Route path='/products' element={<Products/>} />
          <Route path='/products/view/:id' element={<ProductView/>} />
          <Route path='/logout' element={<LogoutPage/>} />
          <Route path='/create' element={<Create/>} />
          <Route path='/update' element={<UpdateProduct/>} />
          <Route path='*' element={<ErrorPage/>} />
        </Routes>
      </Router>
   </UserProvider>
  );
};

export default App;

